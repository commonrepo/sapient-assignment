import React, { useEffect, useState } from "react";
import {
  Grid,
  Paper,
  Select,
  MenuItem,
  TextField,
  InputLabel,
  makeStyles,
  FormControl,
  InputAdornment,
  LinearProgress
} from "@material-ui/core";
import { Search as SearchIcon } from "@material-ui/icons";
import { fetchAllData } from "../api/home";
import Notify from "../components/Notify";
import CardComp from "../components/CardComp";
import Pagination from "@material-ui/lab/Pagination";

const useStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(1),
    backgroundColor: "#1F232A"
  },
  card: { color: "white", backgroundColor: "#333333" },
  image: { height: 310, position: "relative" },
  heroDiv: {
    position: "absolute",
    bottom: 0,
    backgroundColor: "black",
    opacity: 0.7,
    width: "100%",
    padding: theme.spacing(2)
  },
  heroText: { fontSize: "13px" },
  contentRoot: {
    marginTop: theme.spacing(4),
    marginBottom: theme.spacing(4)
  },
  border: { borderBottom: "1px solid #3E3E3E" },
  content: {
    color: "#969696",
    paddingBottom: theme.spacing(1),
    paddingTop: theme.spacing(1)
  },
  span: {
    float: "right",
    color: "#EC8803",
    textAlign: "right",
    paddingBottom: theme.spacing(1),
    paddingTop: theme.spacing(1)
  },
  pagination: { backgroundColor: "white", padding: theme.spacing(2) },
  filter: { padding: theme.spacing(1), margin: theme.spacing(1) }
}));

const Home = () => {
  const [info, setInfo] = useState({});
  const [page, setPage] = useState(1);
  const [results, setResults] = useState([]);
  const [state, setState] = useState({ loading: false, message: "" });
  const [name, setName] = useState("");
  const [status, setStatus] = useState("");
  const [species, setSpecies] = useState("");
  const [gender, setGender] = useState("");

  const handleAction = page => {
    setState({ ...state, loading: true, message: "" });
    fetchAllData(page, name, status, species, gender).then(res => {
      if (res && res.status < 350 && res.data) {
        setInfo(res.data.info);
        setResults(res.data.results);
        setState({ ...state, loading: false, message: "" });
      } else {
        setState({ ...state, loading: false, message: "Something went wrong" });
      }
    });
  };

  useEffect(() => {
    handleAction(page);
  }, [page, name, status, species, gender]);

  const classes = useStyles();

  return (
    <>
      {state.loading && <LinearProgress />}
      {state.message && <Notify message={state.message} />}
      <Paper className={classes.filter}>
        <Grid container spacing={4} justify="flex-end">
          <Grid item xs={12} sm={2}>
            <TextField
              label="Search By Name"
              value={name}
              onChange={e => setName(e.target.value)}
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                )
              }}
            />
          </Grid>
          <Grid item xs={12} sm={2}>
            <TextField
              label="Search By Species"
              value={species}
              onChange={e => setSpecies(e.target.value)}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={2}>
            <FormControl className={classes.formControl} fullWidth>
              <InputLabel id="status-label">Status</InputLabel>
              <Select
                labelId="status-label"
                id="status"
                value={status}
                onChange={e => setStatus(e.target.value)}
              >
                <MenuItem value="">All</MenuItem>
                {["Alive", "Dead", "Unknown"].map(v => (
                  <MenuItem key={v} value={v.toLowerCase()}>
                    {v}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={2}>
            <FormControl className={classes.formControl} fullWidth>
              <InputLabel id="gender-label">Gender</InputLabel>
              <Select
                labelId="gender-label"
                id="gender"
                value={gender}
                onChange={e => setGender(e.target.value)}
              >
                <MenuItem value="">All</MenuItem>
                {["Female", "Male", "Unknown"].map(v => (
                  <MenuItem key={v} value={v.toLowerCase()}>
                    {v}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.root}>
        <Grid container spacing={2} alignItems="stretch">
          {results &&
            results.map(result => (
              <CardComp key={result.id} result={result} classes={classes} />
            ))}
        </Grid>
        <Pagination
          className={classes.pagination}
          page={page}
          showFirstButton
          showLastButton
          count={info.pages || 0}
          color="primary"
          onChange={(e, pageNum) => setPage(pageNum)}
        />
      </Paper>
    </>
  );
};

export default Home;
