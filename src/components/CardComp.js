import React from "react";
import {
  Card,
  Grid,
  CardMedia,
  Typography,
  CardContent
} from "@material-ui/core";

const CardComp = props => {
  const { classes, result } = props;
  return (
    <Grid item xs={6} sm={3}>
      <Card className={classes.card}>
        <CardMedia
          className={classes.image}
          image={result.image}
          title={result.name}
        >
          <div className={classes.heroDiv}>
            <Typography variant="h5" component="h5">
              {result.name}
            </Typography>
            <Typography className={classes.heroText}>{`id: ${
              result.id
            } - created ${new Date(
              new Date() - new Date(result.created)
            ).getFullYear() - 1970} Years ago`}</Typography>
          </div>
        </CardMedia>
        <CardContent className={classes.contentRoot}>
          <Grid container className={classes.border}>
            <Grid item xs={4}>
              <Typography
                variant="caption"
                component="p"
                className={classes.content}
              >
                STATUS
              </Typography>
            </Grid>
            <Grid item xs={8}>
              <Typography
                variant="caption"
                component="p"
                className={classes.span}
              >
                {result.status}
              </Typography>
            </Grid>
          </Grid>
          <Grid container className={classes.border}>
            <Grid item xs={4}>
              <Typography
                variant="caption"
                component="p"
                className={classes.content}
              >
                SPECIES
              </Typography>
            </Grid>
            <Grid item xs={8}>
              <Typography
                variant="caption"
                component="p"
                className={classes.span}
              >
                {result.species}
              </Typography>
            </Grid>
          </Grid>
          <Grid container className={classes.border}>
            <Grid item xs={4}>
              <Typography
                variant="caption"
                component="p"
                className={classes.content}
              >
                GENDER
              </Typography>
            </Grid>
            <Grid item xs={8}>
              <Typography
                variant="caption"
                component="p"
                className={classes.span}
              >
                {result.gender}
              </Typography>
            </Grid>
          </Grid>
          <Grid container className={classes.border}>
            <Grid item xs={4}>
              <Typography
                variant="caption"
                component="p"
                className={classes.content}
              >
                ORIGIN
              </Typography>
            </Grid>
            <Grid item xs={8}>
              <Typography
                variant="caption"
                component="p"
                className={classes.span}
              >
                {result.origin.name}
              </Typography>
            </Grid>
          </Grid>
          <Grid container>
            <Grid item xs={4}>
              <Typography
                variant="caption"
                component="p"
                className={classes.content}
              >
                LAST LOCATION
              </Typography>
            </Grid>
            <Grid item xs={8}>
              <Typography
                variant="caption"
                component="p"
                className={classes.span}
              >
                {result.location.name}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default CardComp;
