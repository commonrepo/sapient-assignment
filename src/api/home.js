import axios from "axios";

export function fetchAllData(page, name, status, species, gender) {
  return axios({
    url: `https://rickandmortyapi.com/api/character/?page=${page}${
      name ? "&name=" + name : ""
    }${status ? "&status=" + status : ""}${
      species ? "&species=" + species : ""
    }${gender ? "&gender=" + gender : ""}`,
    method: "GET",
    headers: {
      "Content-Type": "Application/json"
    }
  })
    .then(res => res)
    .catch(err => err);
}
